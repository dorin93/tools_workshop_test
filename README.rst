=============
Project Title
=============

One Paragraph of the project description goes here.

|

.. contents::

|


Getting Started
===============

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

**Requirements**:

What things you need to install to use the repository.

* Python 3.6
* Tensorflow 1.14
* Python packages as listed in requirements.txt.

**Instructions:**

Recommended way to install the requirements.

.. code:: text

    pip install -r requirements.txt

**Note:**

In the next section there is another option - using this repository as a python package.

|

Use As A Package
================

This repository is a python package, so you can "pip install" it.

User Installation
-----------------

.. code:: text

    pip install git+https://bitbucket.org/razor-tech/REPO_NAME@[version]

Check the latest version under :ref:`Versioning <Versioning>` section.

As part of the installation it installs all prerequisite.

Developer Installation
-----------------
In order to work with this project as a package and still allow easy development, the preferred installation flow is that:

.. code:: text

    git clone https://bitbucket.org/razor-tech/PROJECT_NAME.git

    cd PROJECT_NAME_DIR

    git checkout [your_feature_branch]

    pip install -e .

The "-e" argument enables the developer mode.
It means that each change in your package source code is automatically update the installed package.

That way you can work on infra like any other git repository (and still get the benefits of package usage).

|

Repository Structure
====================
Explain about the project file system structure

.. code:: text

    * src - project source code:
        * data_analysis
        * preprocess
        * evaluation
    * test - all the automatic test are here
    * tools - the entry points to run the repo capabilities:
        * run_preprocess.py - run the process on the data
        * evaluate.py - create evaluation report

    * requirements.txt - python requirements
    * bitbucket-pipelines.yml - CI/CD configuration tool
    * bitbucket-pipelines_requirements.txt - requirements for CI/CD tool
    * pylintrc - configuration file for pylint (static code analysis tool)

|

Running the tests
=================

Explain how to run the automated tests for this system.

.. code:: text

    py.test tests

|

Versioning
=================

Each version is tagged using git-tagging.

You can view the tags in bitbucket website: under "Commits" page,
press "All branches" and choose "Tags" tab (next to "Branches").

Latest version: v0.0.1

|

Authors
=======
* **Razor 770** - *office@razor-labs.com*
