FROM nvcr.io/nvidia/tensorflow:19.10-py3

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
       make \
       g++ \
       sudo

RUN apt-get install -y python3-pip
RUN apt-get install -y python3-dev

ADD requirements.txt /requirements.txt
RUN apt-get update && apt-get install -y libgtk2.0-dev
RUN python3 -m pip install --upgrade pip
ENV PIP_DEFAULT_TIMEOUT 100
RUN  python3 -m pip install -r /requirements.txt
ENV PYTHONIOENCODING=utf-8
