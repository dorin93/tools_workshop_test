
def calculate_quadrilateral_area(pt1, pt2, pt3, pt4):
    """
    Taking four 2D points - each is a list of the form [x, y]
    and returns the area of the this quadrilateral
    """
    x1, y1 = pt1
    x2, y2 = pt2
    x3, y3 = pt3
    x4, y4 = pt4

    area = abs(0.5 * ((x1 * y2 + x2 * y3 + x3 * y4 + x4 * y1) - (x2 * y1 + x3 * y2 + x4 * y3 + x1 * y4)))

    return area