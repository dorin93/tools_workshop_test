class TestClassName(object):
    """ Template class for testing """

    def test_function_name_sanity(self):
        """ Test example function  """
        expected_value = 1

        # run unit under test
        actual_value = 2 - 1

        assert actual_value == expected_value
