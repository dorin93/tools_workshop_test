import argparse
import yaml
from tools_workshop_test.src.calculations import calculate_quadrilateral_area

if __name__ == '__main__':

    # Initiate the parser
    parser = argparse.ArgumentParser(description='Process input')

    parser.add_argument('--yaml_path', help='Path to configuration yaml file', type=str)
    args = parser.parse_args()

    with open(args.yaml_path, 'r') as yaml_file:
        points_dict = yaml.load(yaml_file, Loader=yaml.SafeLoader)

    print(calculate_quadrilateral_area(points_dict['point1'], points_dict['point2'],
                                       points_dict['point3'], points_dict['point4']))
