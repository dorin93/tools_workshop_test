sudo xhost +si:localuser:root
sudo docker run --privileged \
               --entrypoint python3 \
               --network host -it -e DISPLAY=$DISPLAY \
               -v /tmp/.X11-unix/:/tmp/.X11-unix \
               -v /home:/home \
               --ipc="host" \
               --workdir="/home/dorin/PycharmProjects/tools_workshop_test"  \
    tools_workshop_docker:latest tools_workshop_test/tools/calc_runner.py --yaml_path tools/configs/config.yml

sudo xhost -si:localuser:root
