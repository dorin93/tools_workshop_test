sudo xhost +si:localuser:root
sudo docker run --privileged \
               --entrypoint /bin/bash \
               --network host -it -e DISPLAY=$DISPLAY \
               -v /tmp/.X11-unix/:/tmp/.X11-unix \
               -v /home:/home \
               --ipc="host" \
               --workdir="PATH/TO/PROJECT/FOLDER"  \
    docker_template:latest

sudo xhost -si:localuser:root
